#!/bin/bash

DOTFILES=(
	"$HOME/.zshrc"
	"$HOME/.config/nvim/init.vim"
	"$HOME/scripts/updateDotfiles.sh"
)

DOTDIR=(
	"$HOME/.config/neofetch"
	"$HOME/.config/kitty"
	"$HOME/.config/polybar"
	"$HOME/.i3"
)

for i in ${DOTFILES[@]}; do
	cp "$i" "$HOME/repo/dotfiles"
	echo Updated $i
done

for i in ${DOTDIR[@]}; do
	cp "$i" "$HOME/repo/dotfiles" -r
	echo Updated $i
done

exit 0
