#!/bin/sh

secs=15
echo "%{F#fabc2f}Stretch %{F-} 15 min"
while [ "$secs" -gt 0 ]; do
  sleep 60
  echo "%{F#fabc2f}Stretch %{F-} $secs min"
  # printf "\r%02d" $secs
  secs=$(( $secs - 1 ))
  wait
done
echo "%{F#fabc2f}Stretch %{F-} now"
sleep infinity
