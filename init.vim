"
"   ██╗███╗   ██╗██╗████████╗██╗   ██╗██╗███╗   ███╗
"   ██║████╗  ██║██║╚══██╔══╝██║   ██║██║████╗ ████║
"   ██║██╔██╗ ██║██║   ██║   ██║   ██║██║██╔████╔██║
"   ██║██║╚██╗██║██║   ██║   ╚██╗ ██╔╝██║██║╚██╔╝██║
"   ██║██║ ╚████║██║   ██║██╗ ╚████╔╝ ██║██║ ╚═╝ ██║
"   ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝╚═╝  ╚═══╝  ╚═╝╚═╝     ╚═╝
"

set clipboard+=unnamedplus
set number
set autoindent
set smartindent

syntax on

call plug#begin('~/.config/nvim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'Raimondi/delimitMate'
Plug 'ntpeters/vim-better-whitespace'
Plug 'flazz/vim-colorschemes'
Plug 'arcticicestudio/nord-vim'

Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

Plug 'dylanaraps/wal.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'w0rp/ale'
Plug 'junegunn/fzf'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'autozimu/LanguageClient-neovim', { 'branch': 'next', 'do': 'bash install.sh' }
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }

call plug#end()

colorscheme gruvbox
set bg=dark

let g:airline_theme='base16'

let g:NERDTreeMinimalUI=1

let g:better_whitespace_ctermcolor=4
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1
let g:strip_whitespace_confirm=0

set updatetime=100
let g:gitgutter_sign_added='│'
let g:gitgutter_sign_modified='│'
let g:gitgutter_sign_removed='│'
let g:gitgutter_sign_removed_first_line='│'
let g:gitgutter_sign_modified_removed='│'

let g:airline#extensions#ale#enabled=1
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#left_sep=' '
let g:airline#extensions#tabline#left_alt_sep='|'

let g:deoplete#enable_at_startup = 1
autocmd CompleteDone * silent! pclose!
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"

let loaded_matchparen=1


set hidden
let g:LanguageClient_serverCommands = {
    \ 'c'     : ['cquery', '--log-file=/tmp/cq.log', '--init={"cacheDirectory":"/tmp/cquery/"}'],
    \ 'cpp'   : ['cquery', '--log-file=/tmp/cq.log', '--init={"cacheDirectory":"/tmp/cquery/"}'],
    \ 'python': ['pyls']
    \ }
let g:LanguageClient_diagnosticsDisplay = {
    \ 1: {'name': 'Error', 'texthl': 'ALEError', 'signText': '>>', 'signTexthl': 'ALEErrorSign'},
    \ 2: {'name': 'Warning', 'texthl': 'ALEWarning', 'signText': '--', 'signTexthl': 'ALEWarningSign'},
    \ 3: {'name': 'Information', 'texthl': 'ALEInfo', 'signText': '--', 'signTexthl': 'ALEInfoSign'},
    \ 4: {'name': 'Hint', 'texthl': 'ALEInfo', 'signText': '--', 'signTexthl': 'ALEInfoSign'}
    \ }

nnoremap <silent> <C-L>h  :call LanguageClient_textDocument_hover()<CR>
nnoremap <silent> <C-L>rn :call LanguageClient_textDocument_rename()<CR>
nnoremap <silent> <C-L>rf :call LanguageClient_textDocument_references()<CR>
nnoremap <silent> <C-L>ds :call LanguageClient_textDocument_definition({'gotoCmd': 'split'})<CR>
nnoremap <silent> <C-L>dt :call LanguageClient_textDocument_definition({'gotoCmd': 'tabedit'})<CR>

au BufNewFile,BufRead,BufReadPost *.pl set syntax=prolog

map <silent> <C-n> :NERDTreeFocus<CR>
inoremap jk <esc>
